#!/bin/bash


USER=$(ps --no-headers -o user `pgrep -f qtile` | uniq)
XRD="XDG_RUNTIME_DIR=/run/user/$(id -u $USER)"


$(which powertop) --auto-tune
# if [[ -n $(pgrep -f pulseaudio) ]] && su $USER -c "$XRD pulseaudio --kill"


# vim:set ts=4 sw=4 ft=sh et:

