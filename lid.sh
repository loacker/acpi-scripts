#!/bin/bash

DISPLAY=$(basename /tmp/.X11-unix/X0 | tr X :)
USER=$(ps --no-headers -o user `pgrep -f qtile` | uniq)

case "$1" in
    open)
        su $USER -c "DISPLAY=$DISPLAY `which slock`"
    ;;
    close)
        systemctl suspend-then-hibernate
    ;;
esac

# vim:set ts=4 sw=4 ft=sh et:

