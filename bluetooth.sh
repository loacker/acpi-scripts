#!/bin/bash

RFKILL=$(rfkill list bluetooth --output TYPE,DEVICE,SOFT --noheadings | awk '/tpacpi/ {print $3}')
USER=$(ps --no-headers -o user `pgrep -f qtile` | uniq)
XRD="XDG_RUNTIME_DIR=/run/user/$(id -u $USER)"

if [[ $RFKILL == "unblocked" ]]; then
    systemctl start bluetooth
    rfkill unblock | awk '/hci0/ {print $1}' | tr -d ":"
else
    systemctl stop bluetooth
fi


# vim:set ts=4 sw=4 ft=sh et:
