#!/bin/bash

USER=$(ps --no-headers -o user `pgrep -f qtile` | uniq)
XRD="XDG_RUNTIME_DIR=/run/user/$(id -u $USER)"
VOLUME=$(su $USER -c "$XRD pactl list sinks | awk -F'/' '/Volume: f/ {print \$2}'" | tr -d [:space:])
SINK=$(su $USER -c "$XRD pactl list | grep -E 'Sink #.*' | tr -d 'Sink #'")

case "$1" in
    +)
      for s in `echo $SINK`; do
          su $USER -c "$XRD pactl set-sink-volume $s +3%"
      done
    ;;
    -)
      for s in `echo $SINK`; do
          su $USER -c "$XRD pactl set-sink-volume $s -3%"
      done
    ;;
    t)
      for s in $SINK; do
          su $USER -c "$XRD pactl set-sink-mute $s toggle"
      done
    ;;
esac


# vim:set ts=4 sw=4 ft=sh et:
