#!/bin/bash

BRIGHTNESS=/sys/class/backlight/intel_backlight/brightness

case "$1" in 
    +)
        VAL=$(cat $BRIGHTNESS)
        let VAL=$VAL+$(echo "$VAL*10/100" | bc)
        echo $VAL > $BRIGHTNESS
    ;;
    -)
        VAL=$(cat $BRIGHTNESS)
        let VAL=$VAL-$(echo "$VAL*10/100" | bc)
        [[ $VAL -lt 5 ]] && VAL=0 
        echo $VAL > $BRIGHTNESS
    ;;
esac


# vim:set ts=4 sw=4 ft=sh et:
